public interface Employee extends Person {
    void work();
}
