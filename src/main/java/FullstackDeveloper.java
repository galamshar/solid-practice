public class FullstackDeveloper extends Worker implements FrontendDeveloper, BackendDeveloper {
    public FullstackDeveloper(String name) {
        super(name);
    }

    public void develop() {
        writeFront();
        writeBackEnd();
    }

    public void work() {
        develop();
    }

    public void writeFront() {
        System.out.println("My name is " + getName() + ". I`m writing code in JavaScript");
    }

    public void writeBackEnd() {
        System.out.println("My name is " + getName() + ". I`m writing code in Java");
    }
}
